const THREE = require('three');

module.exports = {
	renderScene
};

const epsilonCutoff = 0.00001;
const maxReflectBounces = 40;

function renderScene(scene, cam, options) {
	options = options || {};
	if (!options.width || !options.height)
		options.width = options.height = 64;
	const width = cam.getViewPlaneWidth();
	const height = cam.getViewPlaneHeight();

	// doesn't support arbitrary camera rotations right now
	// assumes view plane is xy plane
	const top = cam.lookAt.y + height/2;
	const right = cam.lookAt.x + width/2;
	const bottom = cam.lookAt.y - height/2;
	const left = cam.lookAt.x - width/2;

	const dx = width/options.width;
	const dy = height/options.height;

	// do cast them rays is for the much well
	var y = top - dy/2; // slight offset to hit the center of the pixel
	var output = new Uint8Array(options.width * options.height * 3);
	var outoffset = 0;
	const ori = cam.lookFrom.clone(); // ray origin
	console.log("Begin raytracing");
	while(y > bottom) {
		var x = left + dx/2; 
		while(x < right) {
			var ray = new THREE.Vector3(x - ori.x, y - ori.y, 0 - ori.z);
			ray.normalize();
			var resultColor = new THREE.Color(0, 0, 0);

			var intersect = getRayIntersect(scene, ray, ori);

			if (intersect) {
				resultColor = colorFromIntersection(scene, intersect);
			} else {
				resultColor = scene.backgroundColor;
			}

			output[outoffset] = Math.floor(resultColor.r * 255);
			output[outoffset+1] = Math.floor(resultColor.g * 255);
			output[outoffset+2] = Math.floor(resultColor.b * 255);
			outoffset += 3;
			x += dx;
		}
		y -= dy;
	}
	console.log("Done");

	return output;
}

function getRayIntersect(scene, ray, ori) {
	var smallestT = Number.MAX_SAFE_INTEGER;
	var hitresult = null;
	scene.spheres.forEach(sph => {
		var result = doRaySphereIntersect(ray, ori, sph, smallestT);
		if (result) {
			smallestT = result.tValue; // we assume the hitresult is only returned if result.tValue is already smaller than smallestT
			hitresult = result;
		}
	});
	scene.tris.forEach(tri => {
		var result = doRayTriIntersect(ray, ori, tri, smallestT);
		if (result) {
			smallestT = result.tValue; // we assume the hitresult is only returned if result.tValue is already smaller than smallestT
			hitresult = result;
		}
	});

	return hitresult;
}

function doRaySphereIntersect(ray, ori, sph, smallestT) {
	var wasInside = false;
	const b = 2 * (
		ray.x*ori.x - ray.x*sph.center.x + 
		ray.y*ori.y - ray.y*sph.center.y +
		ray.z*ori.z - ray.z*sph.center.z
	);
	const c = (
		ori.x*ori.x - 2*ori.x*sph.center.x + sph.center.x*sph.center.x +
		ori.y*ori.y - 2*ori.y*sph.center.y + sph.center.y*sph.center.y +
		ori.z*ori.z - 2*ori.z*sph.center.z + sph.center.z*sph.center.z - 
		sph.radius*sph.radius
	);

	const determinate = b*b - 4*c;
	if (determinate < 0)
		return null; // no intersection

	var t;
	if (determinate === 0) {
		t = -b/2;
	} else {
		const t1 = -b/2 + Math.sqrt(determinate)/2;
		const t2 = -b/2 - Math.sqrt(determinate)/2;
	}

	t = Math.min(t1, t2);
	if (t < 0) {
		t = Math.max(t1, t2); // allow hitting the inside of a sphere
		wasInside = true;
	}
	if (t < 0 || t > smallestT) 
		return null; // intersection irrelevant, sphere is behind or is not first in front

	var hit = ray.clone();
	hit.multiplyScalar(t);
	hit.add(ori);
	var normal = hit.clone();
	normal.sub(sph.center);
	normal.divideScalar(sph.radius);
	wasInside && normal.negate();
	return new HitResult(hit, normal, sph, t, ray, wasInside);
}

function doRayTriIntersect(ray, ori, tri, smallestT) {
	const norm = tri.getNormal(); // may want this to be non-normalized

	/* Doesn't work
	var edge1 = tri.v1.clone().sub(tri.v2);
	var edge2 = tri.v3.clone().sub(tri.v2);
	var p = edge2.clone().cross(ray);
	const determinate = edge1.dot(p);
	if (determinate > -epsilonCutoff || determinate < epsilonCutoff)
		return null; // ray is parallel
	const inverseDet = 1.0/determinate;
	
	// distance from v1 to ray origin
	var tVec = ori.clone().sub(tri.v1);

	// test u parameter
	const u = tVec.dot(p) * inverseDet;
	if (u < 0 || u > 1)
		return null;

	// test v parameter
	var qVec = tVec.clone().cross(edge1);
	const v = qVec.dot(ray) * inverseDet;
	if (v < 0 || u + v > 1)
		return null;

	const t = edge2.dot(qVec) * inverseDet;
	if (t < epsilonCutoff || t > smallestT)
		return null;

	return new HitResult(p, norm, tri, t); // this ray hits the triangle 

	*/


	// check if ray and plane are parallel
	const normDot = -norm.dot(ray); 
	if (Math.abs(normDot) < epsilonCutoff) // almost 0 
		return false; // they are parallel

	var d = norm.dot(tri.v1); 
	t = (norm.dot(ori) - d) / normDot; 
	if (t < 0 || t > smallestT)
		return null; // the triangle is behind 

	// compute the intersection point using equation 1
	var p = ray.clone().multiplyScalar(t).add(ori); 

	var edge1 = tri.v2.clone().sub(tri.v1); 
	var vp1 = p.clone().sub(tri.v1); 
	edge1.cross(vp1); 
	if (norm.dot(edge1) < 0) return null; // P is on the right side 

	var edge2 = tri.v3.clone().sub(tri.v2); 
	var vp2 = p.clone().sub(tri.v2); 
	edge2.cross(vp2); 
	if (norm.dot(edge2) < 0) return null; // P is on the right side 

	var edge3 = tri.v1.clone().sub(tri.v3); 
	var vp3 = p.clone().sub(tri.v3); 
	edge3.cross(vp3); 
	if (norm.dot(edge3) < 0) return null; // P is on the right side 

	return new HitResult(p, norm, tri, t, ray); // this ray hits the triangle 
}

function HitResult(hitPoint, normal, object, t, dir, inside) {
	this.hitPoint = hitPoint;
	this.normal = normal;
	this.hitObj = object;
	this.tValue = t;
	this.rayDir = dir; // direction of the original ray that created this hit
	this.wasInside = inside || false; // cheap hack for sphere refraction calculation
}

function colorFromIntersection(scene, hit, stepsDownRabbitHole) {
	stepsDownRabbitHole = stepsDownRabbitHole || 0;
	var result = new THREE.Color(scene.ambientLight.r, scene.ambientLight.g, scene.ambientLight.b);

	// TODO: allow materials with or without spec/diffuse
	if (hit.hitObj.material.diffuse || hit.hitObj.material.specular) {
		var diffuse = hit.hitObj.material.diffuse.clone();
		result.multiply(diffuse); // multiply ambient light by diffuse
		scene.lights.forEach(light => {
			var shadowIntersect = getRayIntersect(
				scene,
				light.directionTo,
				// offset the new ray origin by a tiny bit to not re-hit this same obj
				hit.hitPoint.clone().add(hit.normal.clone().multiplyScalar(epsilonCutoff))
			);
			if (shadowIntersect)
				return; // add no light, we are in shadow

			const lightNormalDot = hit.normal.dot(light.directionTo);
			if (lightNormalDot > 0) {
				var spec = hit.hitObj.material.specular.clone();

				// calculate reflected light vector
				var reflectVec = hit.normal.clone().multiplyScalar(2);
				reflectVec.multiplyScalar(hit.normal.dot(light.directionTo));
				reflectVec.sub(light.directionTo);

				// eyeReflectDot negated because the rayDir is the opposite of the eye dir
				var eyeReflectDot = Math.max(0, -hit.rayDir.dot(reflectVec));
				spec.multiplyScalar(Math.pow(eyeReflectDot, hit.hitObj.material.phong));

				var diff = diffuse.clone().multiplyScalar(lightNormalDot);

				result.add(diff.add(spec).multiply(light.color));
			}
		});
	} 
	
	// compute reflection material
	if (hit.hitObj.material.reflect) {
		var reflect = hit.hitObj.material.reflect;
		result.r *= (1 - reflect.r);
		result.g *= (1 - reflect.g);
		result.b *= (1 - reflect.b);
		if (stepsDownRabbitHole > maxReflectBounces) {
			result.add(scene.backgroundColor.clone().multiply(reflect));
		} else {
			// calculate reflected eye vector
			var reflectVec = hit.normal.clone().multiplyScalar(2);
			reflectVec.multiplyScalar(-hit.normal.dot(hit.rayDir));
			reflectVec.add(hit.rayDir);

			var reflectIntersect = getRayIntersect(
				scene,
				reflectVec,
				// offset the new ray origin by a tiny bit to not re-hit this same obj
				hit.hitPoint.clone().add(hit.normal.clone().multiplyScalar(epsilonCutoff))
			);

			if (reflectIntersect) {
				result.add(colorFromIntersection(scene, reflectIntersect, stepsDownRabbitHole+1).clone().multiply(reflect));
			} else {
				result.add(scene.backgroundColor.clone().multiply(reflect));
			}
		}
	}

	// compute refraction
	if (hit.hitObj.material.refract) {
		// hacky, check if we are first entering sphere
		const n = (hit.wasInside) ? hit.hitObj.material.ior : 1.0/hit.hitObj.material.ior;
		const c1 = -hit.normal.dot(hit.rayDir);
		const c2 = Math.sqrt(1 - n*n * (1 - c1*c1));
		var refractVec = hit.rayDir.clone().multiplyScalar(n).add(
			hit.normal.clone().multiplyScalar(n * c1 - c2)
		);

		var refractIntersect = getRayIntersect(
			scene,
			refractVec,
			// offset the new ray origin by a tiny bit to not re-hit this same obj
			hit.hitPoint.clone().add(hit.normal.clone().multiplyScalar(-epsilonCutoff))
		);

		if (refractIntersect) {
			result.add(colorFromIntersection(scene, refractIntersect, stepsDownRabbitHole+1).clone().multiply(hit.hitObj.material.refract));
		} else {
			result.add(scene.backgroundColor.clone().multiply(hit.hitObj.material.refract));
		}
	}

	result.r = Math.min(result.r, 1);
	result.g = Math.min(result.g, 1);
	result.b = Math.min(result.b, 1);
	return result;
}

'use strict';
const fs = require('fs');

module.exports = {
	write
};

function write(byteArray, outfile, width, height) {
	console.log('Writing to ppm file');
	const of = fs.createWriteStream(outfile);
	of.write('P3\n' + width + ' ' + height + '\n255');
	for (let i = 0; i < byteArray.length; i++) {
		if (i % width === 0)
			of.write('\n');
		of.write(byteArray[i].toString() + ' ');
	}
	of.end();
}

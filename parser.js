'use strict';

const THREE = require('three');
const fs = require('fs');

const Scene = require('./types/scene.js');
const Sphere = require('./types/sphere.js');
const Tri = require('./types/tri.js');
const Material = require('./types/material.js');
const Light = require('./types/light.js');
const Camera = require('./types/camera.js');

function processSceneFile(file) {
	const filebuff = fs.readFileSync(file, 'utf-8');
	const lines = filebuff.split('\n');
	const cam = new Camera();
	const scene = new Scene();

	lines.forEach(line => {
		switch(line.substr(0, line.indexOf(' '))) {
			case 'CameraLookAt':
				cam.lookAt = vectorFromTokenString(line);
				break;
			case 'CameraLookFrom':
				cam.lookFrom = vectorFromTokenString(line);
				break;
			case 'CameraLookUp':
				cam.lookUp = vectorFromTokenString(line);
				break;
			case 'FieldOfView':
				cam.fov = parseFloat(line.split(' ')[1]);
				break;
			case 'DirectionToLight':
				scene.addLight(processLight(line));
				break;
			case 'AmbientLight':
				scene.ambientLight = colorFromTokenString(line);
				break;
			case 'BackgroundColor':
				scene.backgroundColor = colorFromTokenString(line);
				break;
			case 'Sphere':
				scene.addSphere(processSphere(line)); 
				break;
			case 'Triangle':
				scene.addTri(processTriangle(line)); 
				break;
		}
	});
	scene.mainCam = cam;
	return scene;
}

function vectorFromTokenString(str) {
	var tokens = str.split(' ');
	return new THREE.Vector3(parseFloat(tokens[1]), parseFloat(tokens[2]), parseFloat(tokens[3]));
}

function colorFromTokenString(str) {
	var tokens = str.split(' ');
	return new THREE.Color(parseFloat(tokens[1]), parseFloat(tokens[2]), parseFloat(tokens[3])); 
}

function processLight(lightStr) {
	var tokens = lightStr.split(' ');
	return new Light(
			new THREE.Vector3(parseFloat(tokens[1]), parseFloat(tokens[2]), parseFloat(tokens[3])),
			new THREE.Color(parseFloat(tokens[5]), parseFloat(tokens[6]), parseFloat(tokens[7]))
		);
}

function processSphere(str) {
	var tokens = str.split(' ');
	var sph = new Sphere();
	for(let i = 1; i < tokens.length; i++) {
		switch(tokens[i]) {
			case 'Center':
				sph.center = new THREE.Vector3(parseFloat(tokens[i+1]), parseFloat(tokens[i+2]), parseFloat(tokens[i+3]));
				i += 3;
				break;
			case 'Radius':
				sph.radius = parseFloat(tokens[i+1]);
				i += 1;
				break;
			case 'Material':
				sph.material = processMaterial(tokens.slice(i));
				i += tokens.length;
				break;
		}
	}
	return sph;
}

function processTriangle(str) {
	var tokens = str.split(' ');
	tokens = tokens.filter(t => t);
	var tri = new Tri();
	tri.v1 = new THREE.Vector3(parseFloat(tokens[1]), parseFloat(tokens[2]), parseFloat(tokens[3]));
	tri.v2 = new THREE.Vector3(parseFloat(tokens[4]), parseFloat(tokens[5]), parseFloat(tokens[6]));
	tri.v3 = new THREE.Vector3(parseFloat(tokens[7]), parseFloat(tokens[8]), parseFloat(tokens[9]));
	tri.material = processMaterial(tokens.slice(10));
	return tri;
}

function processMaterial(tokens) {
	var mat = new Material();
	for(let i = 1; i < tokens.length; i++) {
		switch(tokens[i]) {
			case 'Diffuse':
				mat.diffuse = new THREE.Color(parseFloat(tokens[i+1]), parseFloat(tokens[i+2]), parseFloat(tokens[i+3]));
				i += 3;
				break;
			case 'SpecularHighlight':
				mat.specular = new THREE.Color(parseFloat(tokens[i+1]), parseFloat(tokens[i+2]), parseFloat(tokens[i+3]));
				i += 3;
				break;
			case 'Reflective':
				mat.reflect = new THREE.Color(parseFloat(tokens[i+1]), parseFloat(tokens[i+2]), parseFloat(tokens[i+3]));
				i += 3;
				break;
			case 'Refractive':
				mat.refract = new THREE.Color(parseFloat(tokens[i+1]), parseFloat(tokens[i+2]), parseFloat(tokens[i+3]));
				i += 3;
				break;
			case 'PhongConstant':
				mat.phong = parseFloat(tokens[i+1]);
				i += 1;
				break;
			case 'IndexOfRefraction':
				mat.ior = parseFloat(tokens[i+1]);
				i += 1;
				break;
		}
	}
	return mat;
}

module.exports = {
	processSceneFile
}

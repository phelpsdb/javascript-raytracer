const THREE = require('three');

const deg2rad = Math.PI/180;

module.exports = Camera;

function Camera() {
	this.lookAt = new THREE.Vector3(0,0,1);
	this.lookFrom = new THREE.Vector3(0,0,0);
	this.lookUp = new THREE.Vector3(0,1,0);
	this.fov = 30; 
	this.aspect = 1;
}

Camera.prototype.getViewPlaneWidth = function() {
	if (!this.viewPlaneWidth) {
		const adj = new THREE.Vector3().subVectors(this.lookAt, this.lookFrom).length();
		this.viewPlaneWidth = Math.tan(this.fov * deg2rad) * adj * 2;
	}
	return this.viewPlaneWidth;
};

Camera.prototype.getViewPlaneHeight = function() {
	if (!this.viewPlaneHeight) {
		const adj = new THREE.Vector3().subVectors(this.lookAt, this.lookFrom).length();
		return Math.tan(this.fov * deg2rad) * adj * 2;
	}
	return this.viewPlaneHeight;
};

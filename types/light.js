const THREE = require('three');

module.exports = Light;

function Light(directionTo, color) {
	this.directionTo = directionTo;
	this.color = color;
}

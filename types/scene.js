const THREE = require('three');

module.exports = Scene;

function Scene() {
	this.spheres = [];
	this.tris = [];
	this.lights = [];
	this.ambientLight = new THREE.Color(0,0,0);
	this.BackgroundColor = new THREE.Color(0,0,0);
	this.mainCam = null;
}

Scene.prototype.addSphere = function(sphere) {
	this.spheres.push(sphere);
}

Scene.prototype.addTri = function(tri) {
	this.tris.push(tri);
}

Scene.prototype.addLight = function(light) {
	this.lights.push(light);
}

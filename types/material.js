module.exports = Material;

function Material() {
	this.diffuse = null; 
	this.specular = null;
	this.reflect = null;
	this.refract = null;
	this.phong = 0;
	this.ior = 1;
};

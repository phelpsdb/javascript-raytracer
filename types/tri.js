const THREE = require('three');

module.exports = Tri;

function Tri() {
	this.v1 = new THREE.Vector3(0,0,0);
	this.v2 = new THREE.Vector3(0,0,0);
	this.v3 = new THREE.Vector3(0,0,0);
	this.material = null;
	this._normal = null;
}

Tri.prototype.getNormal = function() {
	if (!this._normal) {
		this._normal = new THREE.Vector3();
		this._normal.subVectors(this.v2, this.v1); 
		this._normal.cross(new THREE.Vector3().subVectors(this.v3, this.v1));
		this._normal.normalize();
	}
	return this._normal.clone();
}

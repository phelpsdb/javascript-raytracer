const THREE = require('three');

module.exports = Sphere;

function Sphere() {
	this.center = new THREE.Vector3(0,0,0);
	this.radius = 0;
	this.material = null;
}

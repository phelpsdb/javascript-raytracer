const parser = require('./parser.js');
const ppmWriter = require('./ppmWriter.js');
const raytracer = require('./raytracer.js');

if (process.argv.length < 5) {
	console.log('Usage: node main.js <path-to-scene-file.rayTracing> <output-file.ppm> <size>');
	return 0;
}

const size = parseInt(process.argv[4]);

var scene = parser.processSceneFile(process.argv[2]);
ppmWriter.write(raytracer.renderScene(scene, scene.mainCam, {width:size, height:size}), process.argv[3], size, size);
